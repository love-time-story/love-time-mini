import dayjs from 'dayjs'
import { getOtherHalfInfoBySysID } from "./api/my_v1";
import pubSub from 'pubsub-js'

// app.js
App({
    onLaunch() {
        const that = this;
        // 获取系统信息
        const systemInfo = wx.getSystemInfoSync();
        // 胶囊按钮位置信息
        const menuButtonInfo = wx.getMenuButtonBoundingClientRect();
        // 导航栏高度 = 状态栏高度 + 44
        that.globalData.navInfo.navBarHeight = systemInfo.statusBarHeight + 44;
        that.globalData.navInfo.menuRight = systemInfo.screenWidth - menuButtonInfo.right;
        that.globalData.navInfo.menuBotton = menuButtonInfo.top - systemInfo.statusBarHeight;
        that.globalData.navInfo.menuHeight = menuButtonInfo.height;
    },
    onShow(){
        console.log(this.globalData.userInfo)
        
        if(this.globalData.userInfo.userSysID!==-1){
            let today = dayjs().format("YYYY-MM-DD");
            //登录状态下获取打卡以及情侣数据
            if(this.globalData.refreshDate!==today){
                let reqData = {
                    UserSysID: this.globalData.userInfo.userSysID
                };
                getOtherHalfInfoBySysID(reqData)
                    .then(({code,data})=>{
                        if(code===200){
                            const {
                                SysID=-1,IsSign=0,ContinueSign=0,CouplesMoney=0,
                                IsSingle=0,LoveDate='',OtherHalfNickName='',OtherHalfAvatarUrl=''
                            }=data;
                            if(+SysID!==-1){
                                let otherInfo = {
                                    isSingle: IsSingle,
                                    loveDate: LoveDate,
                                    avatarUrl: OtherHalfAvatarUrl,
                                    nickName: OtherHalfNickName,
                                };
                                let sign = {
                                    isSign: IsSign,
                                    couplesMoney: ContinueSign,
                                    continueSign: CouplesMoney,
                                };
                                wx.setStorageSync('refreshDate',today)
                                wx.setStorageSync("signData", JSON.stringify(sign));
                                wx.setStorage({
                                    key: "otherHalfInfo",
                                    data: JSON.stringify(otherInfo),
                                });
                                this.globalData.refreshDate=today;
                                this.globalData.sign=sign;
                                this.globalData.otherInfo=otherInfo;
                                pubSub.publish('refresh-other-info');
                                pubSub.publish('refresh-sign-data')
                            }
                        }
                    })
            }
        }
    },
    globalData: {
        userInfo: wx.getStorageSync('userInfo')?JSON.parse(wx.getStorageSync('userInfo')): {
            userSysID:-1,
            nickName:'',
            avatarUrl:''
        },
        openId:wx.getStorageSync('openId')?wx.getStorageSync('openId'):'',
        refreshDate:wx.getStorageSync("refreshDate")?wx.getStorageSync("refreshDate"):'',
        sign: wx.getStorageSync("signData") ? JSON.parse(wx.getStorageSync("signData")) : {
            isSign: 0,
            couplesMoney: 0,
            continueSign: 0,
        },
        otherInfo: wx.getStorageSync("otherHalfInfo")?JSON.parse(wx.getStorageSync("otherHalfInfo")) : {
            isSingle: 0,
            loveDate: "",
            avatarUrl: "",
            nickName: ""
        },
        navInfo:{
            navBarHeight: 0, // 导航栏高度
            menuRight: 0, // 胶囊距右方间距（方保持左、右间距一致）
            menuBotton: 0, // 胶囊距底部间距（保持底部间距一致）
            menuHeight: 0, // 胶囊高度（自定义内容可与胶囊高度保证一致）
        }
    }
})
