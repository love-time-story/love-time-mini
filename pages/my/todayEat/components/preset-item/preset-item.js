// pages/my/todayEat/components/preset-item/preset-item.js
Component({
    properties: {
        presetItem:{
            type:Object
        }
    },
    data: {

    },
    methods: {
        onClickDelete(e){
            let id=e.target.dataset.id;
            let detail={
                id:id
            }
            this.triggerEvent('delete-preset',detail)
        },
        onChangeChecked(e){
            let checked=e.detail;
            let id=e.target.dataset.id;
            let detail={
                id:id,
                checked:checked
            }
            this.triggerEvent('change-checked',detail)
        }
    }
})
