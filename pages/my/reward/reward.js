// pages/my/reward/reward.js
import { getUserLimit } from "../../../api/my_v1";
Page({

    /**
     * 页面的初始数据
     */
    data: {
        src:''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        let reqData = {
            LimitName: 'pay',
        }; 
        getUserLimit(reqData)
            .then(({code,data})=>{
                if(code===200){
                    const {limitList=[]}=data;
                    wx.setNavigationBarTitle({
                        title: limitList[0].Title,
                    })
                    this.setData({
                        src:limitList[0].Url
                    })
                }
            })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})