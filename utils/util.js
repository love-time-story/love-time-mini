// const formatTime = date => {
//   const year = date.getFullYear()
//   const month = date.getMonth() + 1
//   const day = date.getDate()
//   const hour = date.getHours()
//   const minute = date.getMinutes()
//   const second = date.getSeconds()

//   return `${[year, month, day].map(formatNumber).join('/')} ${[hour, minute, second].map(formatNumber).join(':')}`
// }

// const formatNumber = n => {
//   n = n.toString()
//   return n[1] ? n : `0${n}`
// }

// module.exports = {
//   formatTime
// }

export const GetUrlParam = (url, name) => {
    let args = url.split("?");
    let retval = null;
  
    if (args[0] == url) {
      /*参数为空*/ return retval; /*无需做任何处理*/
    }
    let str = args[1];
    args = str.split("&");
    for (let i = 0; i < args.length; i++) {
      str = args[i];
      let arg = str.split("=");
      if (arg.length <= 1) {
        continue;
      }
      if (arg[0] == name) {
        retval = arg[1];
      }
    }
    return retval;
};
