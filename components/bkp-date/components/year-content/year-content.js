Component({
  /**
   * 组件的属性列表
   */
  properties: {
    yearList: {
      type: Array,
      value: [],
    },
  },

  /**
   * 组件的初始数据
   */
  data: {},

  /**
   * 组件的方法列表
   */
  methods: {
    onClickYear(e) {
      let detail = {
        year: e.currentTarget.dataset.year,
      };
      this.triggerEvent("click-year", detail);
    },
  },
});
