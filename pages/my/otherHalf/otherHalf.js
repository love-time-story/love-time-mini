const app = getApp();

import {getUserLoveCode,getUserInfoByLoveCode,bindUserOtherHalf} from '../../../api/my_v1'

import Notify from "@vant/weapp/notify/notify";
Page({
  data: {
    app: app,
    inviteCode: '暂无数据',
    otherHalfCode: "",
    error: false,
    otherHalfInfo: {
      show: false,
      userSysID: -1,
      avatarUrl: "",
      nickName: "",
    },
    loveDate: {
      show: false,
      date: "",
      error: false,
    },
    onBeforeClose: function (action) {
      return false;
    },
  },
  onShow() {
    this.GetUserLoveCode();
  },
  GetUserLoveCode(){
    let reqData = {
        UserSysID: app.globalData.userInfo.userSysID,
    };
    getUserLoveCode(reqData)
        .then(({code,data})=>{
            if(code===200){
                let { LoveCode } = data;
                this.setData({
                    inviteCode: LoveCode,
                });
            }
        })

  },
  onChangeCode(event) {
    this.setData({
      error: false,
      otherHalfCode: event.detail.trim(),
    });
  },
  onClickValidateHalf() {
    if (this.data.otherHalfCode === "") {
      this.setData({
        error: true,
      });
    } else if (this.data.otherHalfCode == this.data.inviteCode) {
      Notify({ type: "danger", message: "不允许输入自己的邀请码!" });
    } else {
      let reqData = {
        LoveCode: this.data.otherHalfCode,
      };
      getUserInfoByLoveCode(reqData)
        .then(({code,data,msg})=>{
            if(code===200){
                let { UserSysID, NickName, AvatarUrl } = data;
                let otherHalfInfo = {
                  show: true,
                  userSysID: UserSysID,
                  avatarUrl: AvatarUrl,
                  nickName: NickName,
                };
                this.setData({
                  otherHalfInfo: otherHalfInfo,
                });
            }
            else{
                Notify({ type: "danger", message: msg });
            }
        })
    }
  },
  onCancelOtherHalf() {
    let otherHalfInfo = {
      show: false,
      userSysID: -1,
      avatarUrl: "",
      nickName: "",
    };
    let loveDate = {
      show: false,
      date: "",
      error: false,
    };
    this.setData({
      otherHalfInfo: otherHalfInfo,
      loveDate: loveDate,
    });
  },

  onConfirmOtherHalf() {
    if (this.data.loveDate.date === "") {
      this.setData({
        "loveDate.error": true,
      });
    } else {
      let data = {
        UserSysID: app.globalData.userInfo.userSysID,
        OtherHalfUSerID: this.data.otherHalfInfo.userSysID,
        LoveDate: this.data.loveDate.date,
      };
      bindUserOtherHalf(data).then(({ code, data, msg }) => {
        if (code === 200) {
            wx.clearStorageSync('otherHalfInfo')
          wx.switchTab({
            url: "../../my/index",
          });
        } else {
          Notify({ type: "danger", message: msg });
        }
      });
    }
  },

  onClickSkip() {
    wx.switchTab({
      url: "../../loveDay/index",
    });
  },

  //选择相恋日期
  onClickSelectLoveDate() {
    this.setData({
      "loveDate.show": true,
    });
  },
  onConfirmDay(e) {
    let loveDate = {
      show: false,
      date: e.detail.dayInfo.date,
      error: false,
    };
    this.setData({
      loveDate: loveDate,
    });
  },
  onHideDate() {
    this.setData({
      "loveDate.show": false,
    });
  },
});
