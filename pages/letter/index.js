import { getLetterCode } from "../../api/letter";
import { getUserLetterList } from "../../api/letter_v1";
import Notify from "@vant/weapp/notify/notify";

const app=getApp()

Page({
    data: {
        app:app,
        baseUrl:'',
        active:'send',
        letterList:[]
    },
    onLoad(options) {
        let reqData={
            Type:'send',
            UserSysID:app.globalData.userInfo.userSysID
        }
        this.GetUserLetterList(reqData);
    },
    onShow(){
        // getLetterCode()
        //     .then(res=>{
        //         let data=wx.arrayBufferToBase64(res.data.res.data.data);
        //         console.log(data)
        //         this.setData({
        //             baseUrl:data
        //         })
        //     })
    },
    onClickDetail(e){
        const SysID=e.currentTarget.dataset.sysid;
        wx.navigateTo({
          url: './letter-detail/letter-detail?sysID='+SysID,
        })
    },
    onChange(e){
        const active=e.detail.name;
        this.setData({
            active:active
        })
        const reqData={
            Type:active,
            UserSysID:app.globalData.userInfo.userSysID
        }
        this.GetUserLetterList(reqData)
    },
    GetUserLetterList(reqData){
        getUserLetterList(reqData)
            .then(({code,data,msg})=>{
                if (code===200) {
                    this.setData({
                        letterList:data.list
                    })
                }
                else{
                    this.setData({
                        letterList:[]
                    })
                    Notify({ type: "danger", message: msg });
                }
            })
    },
    onClickAdd(){
        wx.navigateTo({
          url: './edit-letter-new/edit-letter',
        })
    }
 
})