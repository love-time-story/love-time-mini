Component({
    properties: {
        show: {
            type: Boolean,
            value: false,
        },
        showCancelBtn:{
            type: Boolean,
            value: true,
        }
    },
    data: {

    },
    methods: {
        onClickCancel(){
            this.triggerEvent("click-cancel");
        },
        onClickConfirm(){
            this.triggerEvent("click-confirm")
        }
    }
})
