// pages/list/components/list-content/list-content.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        littleThings:{
            type:Array,
            value:[]
        }
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {
        onClickLittleThing(e){
            let littleThing=e.currentTarget.dataset.thing;
            wx.navigateTo({
              url: './complete-little-thing/complete-little-thing?littleThing='+JSON.stringify(littleThing),
            })
        }
    }
})
