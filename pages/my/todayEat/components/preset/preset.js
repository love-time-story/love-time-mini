import Notify from "@vant/weapp/notify/notify"
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        presetList:{
            type:Array,
            value:[]
        }
    },
    data: {
        addVisible:false,
        add:{
            name:'',
            breakfast:false,
            lunch:false,
            afternoonTea:false,
            dinner:false,
            midnightSnack:false
        }
    },
    methods: {
        onClickAddPreset(){
            this.setData({
                addVisible:true
            })
        },
        onCloseAddPreset(){
            this.setData({
                addVisible:false,
                add:{
                    name:'',
                    breakfast:false,
                    lunch:false,
                    afternoonTea:false,
                    dinner:false,
                    midnightSnack:false
                }
            })
        },
        onChangeName(e){
            this.data.add.name=e.detail
        },
        onChangeTag(e){
            let tag=e.target.dataset.tag;
            switch (tag) {
                case 'breakfast':
                    this.setData({
                        'add.breakfast':!this.data.add.breakfast
                    })
                    break;
                case 'lunch':
                    this.setData({
                        'add.lunch':!this.data.add.lunch
                    })
                    break;
                case 'afternoonTea':
                    this.setData({
                        'add.afternoonTea':!this.data.add.afternoonTea
                    })
                    break;
                case 'dinner':
                    this.setData({
                        'add.dinner':!this.data.add.dinner
                    })
                    break;
                case 'midnightSnack':
                    this.setData({
                        'add.midnightSnack':!this.data.add.midnightSnack
                    })
                    break;
                default:
                    break;
            }
        },
        onClickDeletePreset(e){
            let id=e.detail.id;
            let detail={
                id:id
            }
            this.triggerEvent('delete-preset',detail)
        },
        onChangeChecked(e){
            let detail=e.detail;
            this.triggerEvent('change-checked',detail)
        },
        onClickSavePreset(){
            if(this.data.add.name===''){
                Notify({ type: "danger", message: "菜名不能为空!" });
            }
            else{
                let detail={
                    id:'-1',
                    checked:true,
                    name:this.data.add.name,
                    breakfast:this.data.add.breakfast,
                    lunch:this.data.add.lunch,
                    afternoonTea:this.data.add.afternoonTea,
                    dinner:this.data.add.dinner,
                    midnightSnack:this.data.add.midnightSnack,
                }
                detail.id=(this.properties.presetList.length+1)+'';
                this.triggerEvent('save-preset',detail)
                this.onCloseAddPreset()
            }
            
        }
    }
})
