// pages/my/components/head-item/head-item.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        name:{
            type:String,
            value:'情侣币'
        },
        number:{
            type:Number,
            value:0
        }
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {

    }
})
