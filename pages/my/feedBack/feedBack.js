import {throttle} from '../../../utils/throttle_debounce'
import Notify from '@vant/weapp/notify/notify';
import {submitUserFeedBack} from '../../../api/my_v1'
const app = getApp();
Page({
    data: {
        app:app,
        fieldAutoSize:{
            minHeight: 60
        },
        fileList:[],
        loading:false,
        value:'',
        disabled:true
    },
    afterRead(data){
        let file=data.detail.file
        let fileList=this.data.fileList;
        file.forEach(item=>{
            if(fileList.length<10){
                fileList.push(item)
            }
        })
        this.setData({
            fileList:fileList
        })
    },
    onChangeValue:throttle(function(e){
        this.data.value=e.detail;
        this.setData({
            disabled:this.data.value.trim()===''
        })
    },500),
    async onClickSave(){
        this.setData({
            loading:true
        })
        let fileList= await this.GetImageList();
        let data={
            UserSysID:app.globalData.userInfo.userSysID,
            Content:this.data.value,
            ImageData:fileList
        }
        submitUserFeedBack(data)
            .then(({code})=>{
                this.setData({
                    loading:false
                })
                if(code===200){
                    wx.navigateBack({
                        delta:1
                    })
                }
                else{
                    Notify({ type: 'danger', message: '保存失败!' });
                }
            })
            .catch(()=>{
                this.setData({
                    loading:false
                })
            })
    },
    async GetImageList(){
        let filePromise=this.data.fileList.map(async item=>{
            const response = await this.UploadImgToBase64(item.url);
            let {data}=response;
            return data
        })
        let fileList = [];
        for (let textPromise of filePromise) {
            fileList.push(await textPromise)
        };
        return fileList
    },
    UploadImgToBase64(file) {
        return new Promise((resolve,reject) => {
            wx.getFileSystemManager()
                .readFile({
                    filePath: file,
                    encoding: 'base64',
                    success: res => {
                        return resolve(res)
                    },
                    fail(res) {
                        return reject(res)
                    }
                })
        })
    },
})