import dayjs from "dayjs";
import calendar from '../../static/js/calendar'
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    show: {
      type: Boolean,
      value: false,
    },
    showLunar: {
      type: Boolean,
      value: true,
    },
    lunar: {
      type: Boolean,
      value: false,
    },
    maskClosable: {
      //点击蒙层是否允许关闭
      type: Boolean,
      value: true,
    },
    value: {
      type: String,
      value: "",
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    type: "day",
    headerYear: "",
    headerMonth: "",
    yearList: [],
    year: "",
    month: "",
    day: "",
    beginDay: "",
  },
  observers: {
    show: function () {
      if (this.properties.show) {
        let initDate = this.properties.value
          ? this.properties.value
          : dayjs().format("YYYY-MM-DD");
        let firstDay = dayjs(initDate).format("YYYY-MM") + "-01";
        let week = dayjs(firstDay).day();
        let beginDay = dayjs(firstDay)
          .add(week === 0 ? -6 : 1 - week, "day")
          .format("YYYY-MM-DD");
        this.setData({
          headerYear: dayjs(initDate).year() + "年",
          headerMonth: dayjs(initDate).month() + 1 + "月",
          year: dayjs(initDate).year(),
          month: dayjs(initDate).month() + 1,
          beginDay: beginDay,
        });
      }
    },
  },

  /**
   * 组件的方法列表
   */
  methods: {
    onChangeType(e) {
      let type = e.currentTarget.dataset.type;
      if (type !== this.data.type) {
        switch (type) {
          case "year":
            let year = this.data.headerYear.substring(0, 3);
            let minYear = +(year + "0");
            let maxYear = minYear + 9;
            let yearList = [];
            for (let i = minYear - 1; i < maxYear + 2; i++) {
              yearList.push(i);
            }
            this.setData({
              type: type,
              headerYear: minYear + " - " + maxYear,
              year: "",
              headerMonth: "",
              month: "",
              yearList: yearList,
            });
            break;
          case "month":
            this.setData({
              type: type,
              headerMonth: "",
              month: "",
            });
            break;
          default:
            break;
        }
      }
    },

    onChangeLunar(e) {
      let detail = {
        lunar: e.currentTarget.dataset.lunar,
      };
      this.triggerEvent("change-lunar", detail);
    },
    onClickHide() {
      this.triggerEvent("click-hide");
    },
    onClickCatch() {
      // console.log('10101')
    },
    onClickDay(e) {
      let detail = {
        dayInfo: e.detail.dayInfo,
      };
      this.triggerEvent("confirm-day", detail);
    },
    onClickToday(){
        let { IMonthCn = "", IDayCn = "" } = calendar.solar2lunar(
            dayjs().format("YYYY"),
            dayjs().format("MM"),
            dayjs().format("DD")
        );
        let dayInfo={
            date:dayjs().format('YYYY-MM-DD'),
            isCurrentMonth:true,
            lunarDate:IMonthCn+IDayCn,
            lunarDay:IDayCn,
            solarDay:+dayjs().format('DD'),
            today:true
        }
        let detail = {
            dayInfo: dayInfo,
        };
        this.triggerEvent("confirm-day", detail);
    },
    //双击向左
    onClickDoubleLeft() {
      let year = 0;
      let minYear = 0;
      let maxYear = 0;
      switch (this.data.type) {
        case "year":
          year = +this.data.headerYear.substring(0, 4);
          minYear = year - 10;
          maxYear = minYear + 9;
          let yearList = [];
          for (let i = minYear - 1; i < maxYear + 2; i++) {
            yearList.push(i);
          }

          this.setData({
            headerYear: minYear + " - " + maxYear,
            yearList: yearList,
          });
          break;
        case "month":
          year = +this.data.headerYear.substring(0, 4);
          this.setData({
            headerYear: year - 1 + "年",
            year: year,
          });
        case "day":
          year = +this.data.year;
          this.setData({
            year: year - 1,
            headerYear: year - 1 + "年",
            beginDay: this.conputeBeginDay(
              year - 1 + "-" + this.data.month + "-01"
            ),
          });
          break;
        default:
          break;
      }
    },
    //双击向右
    onClickDoubleRight() {
      let year = 0;
      let minYear = 0;
      let maxYear = 0;
      switch (this.data.type) {
        case "year":
          year = +this.data.headerYear.substring(7, 11);
          minYear = year + 1;
          maxYear = minYear + 9;
          let yearList = [];
          for (let i = minYear - 1; i < maxYear + 2; i++) {
            yearList.push(i);
          }
          this.setData({
            headerYear: minYear + " - " + maxYear,
            yearList: yearList,
          });
          break;
        case "month":
          year = +this.data.headerYear.substring(0, 4);
          this.setData({
            headerYear: year + 1 + "年",
            year: year,
          });
          break;
        case "day":
          year = +this.data.year;
          this.setData({
            year: year + 1,
            headerYear: year + 1 + "年",
            beginDay: this.conputeBeginDay(
              year + 1 + "-" + this.data.month + "-01"
            ),
          });
          break;
        default:
          break;
      }
    },
    //单击向左
    onClickLeft() {
      if (this.data.type === "day") {
        if (+this.data.month === 1) {
          let year = +this.data.year;
          this.setData({
            month: 12,
            headerMonth: "12月",
            year: year - 1,
            headerYear: year - 1 + "年",
            beginDay: this.conputeBeginDay(year - 1 + "-12-01"),
          });
        } else {
          let month = +this.data.month;
          this.setData({
            month: month - 1,
            headerMonth: month - 1 + "月",
            beginDay: this.conputeBeginDay(
              this.data.year + "-" + (month - 1) + "-01"
            ),
          });
        }
      }
    },
    //单击向右
    onClickRight() {
      if (this.data.type === "day") {
        if (+this.data.month === 12) {
          let year = +this.data.year;
          this.setData({
            month: 1,
            headerMonth: "1月",
            year: year + 1,
            headerYear: year + 1 + "年",
            beginDay: this.conputeBeginDay(year + 1 + "-01-01"),
          });
        } else {
          let month = +this.data.month;
          this.setData({
            month: month + 1,
            headerMonth: month + 1 + "月",
            beginDay: this.conputeBeginDay(
              this.data.year + "-" + (month + 1) + "-01"
            ),
          });
        }
      }
    },
    //点击选中了年
    onClickYear(e) {
      this.setData({
        year: e.detail.year,
        headerYear: e.detail.year + "年",
        type: "month",
      });
    },
    //点击选中了月
    onClickMonth(e) {
      this.setData({
        month: e.detail.month,
        headerMonth: e.detail.month + "月",
        type: "day",
        beginDay: this.conputeBeginDay(
          this.data.year + "-" + e.detail.month + "-" + "01"
        ),
      });
    },
    conputeBeginDay(date) {
      let week = dayjs(date).day();
      let beginDay = dayjs(date)
        .add(week === 0 ? -6 : 1 - week, "day")
        .format("YYYY-MM-DD");
      return beginDay;
    },
  },
});
