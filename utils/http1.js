const BASE_URL = "http://127.0.0.1:9110";
const get = (url, data = {}, config = {}) => {
  return new Promise((resolve, reject) => {
    wx.request({
      url: BASE_URL + url,
      data: data,
      method: "GET",
      ...config,
      success: function (res) {
        resolve(res.data);
      },
      fail: function (err) {
        reject(err);
      },
    });
  });
};

const post = (url, data = {}, config = {}) => {
  return new Promise((resolve, reject) => {
    wx.request({
      url: BASE_URL + url,
      data: data,
      method: "POST",
      ...config,
      success: function (res) {
        resolve(res.data);
      },
      fail: function (err) {
        reject(err);
      },
    });
  });
};
export const axios = { get, post };
