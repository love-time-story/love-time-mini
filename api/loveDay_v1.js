import {axios} from '../utils/http1'

export const getLoveDayList=(reqData)=>{
    return axios.get('/v1/wechat/loveDay/list',reqData)
}

export const handleLoveDay=(reqData)=>{
    return axios.post('/v1/wechat/loveDay/handleLoveDay',reqData)
}