// pages/letter/edit-letter/edit-letter.js
import {insertLetterImage,getLetterCode} from '../../../api/letter'
let richText = null;  //富文本编辑器实例
Page({
    data: {
        readOnly:false,
        placeholder:'请输入...'
    },

    //清空编辑器内容之前的回调
    clearBeforeEvent() {
        
    },

    //清空编辑器内容成功时回调
    clearSuccess(){

    },

    //撤销内容成功时回调
    undo(){

    },

    //恢复内容成功时回调
    restore(){

    },

    //编辑器初始化完成时回调，可以获取组件实例
    onEditorReady(){
        richText = this.selectComponent('#richText'); //获取组件实例
    },

     insertImageEvent(){
        wx.chooseMedia({
            count: 1,
            mediaType:['image'],
            success: async res => {
                console.log(res)
                const response = await this.UploadImgToBase64(res.tempFiles[0].tempFilePath);
                let reqData={
                    ImageData:response.data
                }
                insertLetterImage(reqData)
                    .then(({code,data})=>{
                        if(code===200){
                            richText.insertImageMethod(data.ImageUrl)
                                .then(res => {
                                    console.log('[insert image success callback]=>', res)
                                }).catch(res => {
                                    console.log('[insert image fail callback]=>', res)
                                });
                        }
                    })
            }
          })
    },
    getEditorContent(value){
        let html=value.detail.value.html;
        let reaData={
            UserSysID:'26',
            ToName:'阿珂',
            FromName:'墨白',
            Content:html
        }
        getLetterCode(reaData)
            .then(res=>{

            })
    },
    UploadImgToBase64(file) {
        return new Promise((resolve, reject) => {
          wx.getFileSystemManager().readFile({
            filePath: file,
            encoding: "base64",
            success: (res) => {
              return resolve(res);
            },
            fail(res) {
              return reject(res);
            },
          });
        });
    },
})